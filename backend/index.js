//import bodyParser from 'body-parser';
import express from 'express';
import mongoose from 'mongoose';
import routes from './src/routes/antiracismRoutes';


const app = express();
const PORT = 4000;
app.get('/', (req, res) =>
  res.send(`<title>Node and Express</title>
            <h2>Node and express server running on port ${PORT}</h2>`
  )
)
app.listen(PORT, () =>
  console.log(`Your server is running on port ${PORT}`))


//mongoose connection
mongoose.Promise = global.Promise
mongoose.connect('mongodb+srv://root:root@cluster0.5rpyn.mongodb.net/antiracismdb', {
  // useNewUrlParser: true,
  // useUnifiedTopology: true  not necessary .only used in older versions
})
//bodyParser setup

app.use(express.urlencoded());
app.use(express.json())

app.use((req,res,next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods','GET,DELETE')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  
  next();
})

routes(app)