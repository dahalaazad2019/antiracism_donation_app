import { addNewDonation, deleteDonation, getDonations, updateDonation } from "../controllers/antiracismController"



const routes = (app) => {
    //create routes for donations
    app.route('/donations')
        //create get request
        .get(getDonations)
            
        //create post request
        .post(addNewDonation);
    
    //put request based on different donations
    app.route('/donations/:donationID')
    .put(updateDonation)
    .delete(deleteDonation)
}

export default routes
