import { useEffect, useState } from 'react';
import './App.css';
import { DonationForm } from './Components/Donation/DonationForm';
import { DonationList } from './Components/Donation/DonationList';
//import axios from 'axios';
import { DeleteDonations, GetDonations, PostDonations } from './API/dontation';
import axios from 'axios';

function App() {

  const [donations, setDonations] = useState([]);
  const [currentDonation, setCurrentDonation] = useState({})

  useEffect(async () => {
    await loadAllDonations();
  }, [])


  const loadAllDonations = async () => {
    const res = await GetDonations()
    setDonations(res.data);

  }

  const submitDonation = async (donation,e) => {
    e.preventDefault()
    // axios.post('http://localhost:4000/donations',
    //   donation)
    try {
      await PostDonations(donation);
      await loadAllDonations();
    } catch (e) {
      console.log(e);
    }


  }

  const deleteDonation = async (donationId) => {
    console.log(donationId);
    // await axios.delete(`http://localhost:4000/donations/${[donationId]}`)
    await DeleteDonations (donationId)
    await loadAllDonations()
  }
  // const updateCurrentDonation= (item) => {
  //   setCurrentDonation(item)
  // } 




  return (
    <div className="App">
      <div className="container-fluid">
        <div className="row">
          <div className="col s10"><DonationList donations={donations}
                                                 updateCurrentDonation={setCurrentDonation}
                                                deleteDonation={deleteDonation}
          />
          </div>
        </div>
        <div className="row">
          <div className="col s10"><DonationForm submitDonation={submitDonation} /></div>
        </div>
      </div>
    </div>
  );
}

export default App;
