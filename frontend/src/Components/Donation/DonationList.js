import React, { useEffect, useState } from 'react'

export const DonationList = ({ donations, updateCurrentDonation,deleteDonation }) => {
   

  const [donList,setDonList] = useState([]);

  useEffect(()=>{
    setDonList([...donations]);
    console.log('changed donation')
  },[donations]);

  return (
    <div>
      <h4 className='center'>Donations List</h4>
      <ul className="collection">
        {donList.map((item,index) => (
          <div className="collection-item"
            style={{display:'flex',justifyContent:'space-between'}}
            key={item._id}
            onClick={() => updateCurrentDonation(item)}>
            <div className="data">
            {item.organizationName}-${item.dollarAmount}
            <li>{item.comment}</li>
            </div>
            {/* {console.log(donList)} */}
            
            <div className="buttons flex">
              <button>Edit</button>
              <button onClick={() => deleteDonation(donList[index]._id)}>Delete</button>
              {/* {console.log(donList[index]._id)} */}
            </div>
          </div>
        ))}
      </ul>
      <h5>Total: ${donList.reduce((totalDonations, donation) => totalDonations + donation.dollarAmount, 0)}</h5>
    </div>
  )
}

