//import axios from 'axios'
import React, { useState } from 'react'
import { PostDonations } from '../../API/dontation';

export const DonationForm = ({submitDonation}) => {
  const [donation, setDonation] = useState({
    organizationName: '',
    dollarAmount: '',
    comment: ''
  });

  const inputHandler = (e) => {
    setDonation({
      ...donation,
      [e.target.name]: e.target.value
      
    });
    
    
  }


  return (
    <>
      <h4 className="">Donation Form</h4>
        <form onSubmit={()=>submitDonation(donation)}>
        <div className="row">
        <div className="col 12">
          <div className="row">
            <div className="input-field col s6">
              <input onChange={inputHandler} type="text" placeholder='Organization Name' name="organizationName" />
              {/* <label >Organization Name</label> */}
            </div>
            <div className="input-field col s6">
              <input onChange={inputHandler} type="text" placeholder='Dollar Amount' name="dollarAmount" />
              {/* <label>Dollar Amount</label> */}
            </div>
            <div className="input-field col s6">
              <input onChange={inputHandler} type="text" placeholder='Comment' name="comment" />
              {/* <label>Comment</label> */}
            </div>
          </div>
          <div className="button-area">
            <button type='submit'>Submit</button>
          </div>
        </div>
      </div>
        </form>
      </>
  )
}
