//axios---->2
import donationAxios from "./axios";


export const GetRequest=(url,config) => {
    return donationAxios.get(url,config)
}
export const PostRequest=(url,config) => {
    return donationAxios.post(url,config)
}
// export const PutRequest=(url,config) => {
//     return donationAxios.put(url,config)
// }
export const DeleteRequest=(url,config) => {
    return donationAxios.delete(url,config)
}

