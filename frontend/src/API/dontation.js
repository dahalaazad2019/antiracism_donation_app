//axios---->3

import { DeleteRequest, GetRequest, PostRequest } from "../plugins/https"


export const GetDonations = () => {
    return GetRequest('donations');
}

export const PostDonations = (data) => {
    return PostRequest('donations',data)
}

export const DeleteDonations = (id) => {
    return DeleteRequest(`donations/${id}`)
}